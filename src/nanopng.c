/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdio.h>
#include <string.h>

#include "nanopng.h"
#include "nanopng_source.h"
#include "nanopng_chunk.h"

static const uint8_t png_magic[8] = { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };

int nanopng_load_from_file(nanopng_t *nanopng_ptr, char *filename)
{
    FILE *file = fopen(filename, "r");

    if (file == NULL)
        return NANOPNG_EFILE;

    nanopng_file_t file_source;
    nanopng_file_to_source(&file_source, file);

    int code = nanopng_load(nanopng_ptr, (nanopng_source_t *)&file_source);
    return code;
}

int nanopng_load_from_buffer(nanopng_t *nanopng_ptr, nanopng_buffer_t buffer)
{
    nanopng_buffer_src_t buffer_src;
    nanopng_buffer_to_source(&buffer_src, buffer);

    return nanopng_load(nanopng_ptr, (nanopng_source_t *)&buffer_src);
}

int nanopng_load(nanopng_t *nanopng_ptr, nanopng_source_t *source)
{
    if (nanopng_check_source(source))
        /* Source is invalid */
        return NANOPNG_ESOURCE;

    memset(nanopng_ptr, 0, sizeof(nanopng_t));

    /* Check magic code */
    uint8_t source_magic[sizeof(png_magic)];

    if (nanopng_source_read_block(source, sizeof(png_magic), source_magic))
        /* Incomplete magic code */
        return NANOPNG_NOTAPNG;

    if (memcmp(source_magic, png_magic, sizeof(png_magic)) != 0)
        /* Invalid magic code */
        return NANOPNG_NOTAPNG;

    /* Add other things... */

    return NANOPNG_OK;
}

void nanopng_unload(nanopng_t *nanopng_ptr)
{

}
