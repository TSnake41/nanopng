/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#ifndef H_NANOPNG_SOURCE
#define H_NANOPNG_SOURCE

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "nanopng_structs.h"

#define NANOPNG_SOURCE_BUFFER 0
#define NANOPNG_SOURCE_FILE 1

bool nanopng_check_source(nanopng_source_t *source);
bool nanopng_source_read_block(nanopng_source_t *source, size_t count, void *block);
bool nanopng_source_read_uint32(nanopng_source_t *source, uint_least32_t *val);
void nanopng_file_to_source(nanopng_file_t *source, FILE *file);
void nanopng_buffer_to_source(nanopng_buffer_src_t *source, nanopng_buffer_t buffer);

#endif /* H_NANOPNG_SOURCE */
