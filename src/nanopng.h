/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#ifndef H_NANOPNG
#define H_NANOPNG

#include <stdlib.h>
#include <stdint.h>

#include "nanopng_structs.h"

/* Error codes */
#define NANOPNG_OK 0
#define NANOPNG_EFAILED 1
#define NANOPNG_ENOMEM 2
#define NANOPNG_ENODATA 3
#define NANOPNG_EFILE 4
#define NANOPNG_ESOURCE 5
#define NANOPNG_NOTAPNG 6
#define NANOPNG_EUNMANAGABLE 7

int nanopng_load_from_file(nanopng_t *nanopng_ptr, char *filename);
int nanopng_load_from_buffer(nanopng_t *nanopng_ptr, nanopng_buffer_t buffer);
int nanopng_load(nanopng_t *nanopng_ptr, nanopng_source_t *source);
void nanopng_unload(nanopng_t *nanopng_ptr);

#endif /* H_NANOPNG */
