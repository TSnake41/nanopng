/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "nanopng_source.h"

bool nanopng_check_source(nanopng_source_t *source)
{
    if (source->type == NANOPNG_SOURCE_BUFFER) {
        nanopng_buffer_src_t *buffer_src = (nanopng_buffer_src_t *)source;
        nanopng_buffer_t buffer = buffer_src->buffer;

        if (buffer.size == 0 || buffer.block == NULL)
            return true;

    } else if (source->type == NANOPNG_SOURCE_FILE) {
        nanopng_file_t *file_source = (nanopng_file_t *)source;

        if (file_source->file == NULL)
            return true;

    } else
        return true;

    return false;
}

bool nanopng_source_read_block(nanopng_source_t *source, size_t count, void *block)
{
    if (source->type == NANOPNG_SOURCE_BUFFER) {
        nanopng_buffer_src_t *buffer_src = (nanopng_buffer_src_t *)source;
        nanopng_buffer_t buffer = buffer_src->buffer;

        if (buffer_src->position + count > buffer.size)
            /* Out of bound */
            return true;

        /* Read data */
        memcpy(block, (char *)buffer.block + buffer_src->position, count);

        /* Update position */
        buffer_src->position += count;

    } else if (source->type == NANOPNG_SOURCE_FILE) {
        nanopng_file_t *file_source = (nanopng_file_t *)source;

        if (fread(block, count, 1, file_source->file) == 0)
            /* No enough data available */
            return true;

    } else
        /* Invalid source */
        return true;

    return false;
}

bool nanopng_source_read_uint32(nanopng_source_t *source, uint_least32_t *val)
{
    uint8_t bytes[4];

    if (nanopng_source_read_block(source, 4, bytes))
        return true;

    *val = bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3];
    return false;
}

void nanopng_file_to_source(nanopng_file_t *source, FILE *file)
{
    source->base.type = NANOPNG_SOURCE_FILE;
    source->file = file;
}

void nanopng_buffer_to_source(nanopng_buffer_src_t *source, nanopng_buffer_t buffer)
{
    source->base.type = NANOPNG_SOURCE_BUFFER;
    source->buffer = buffer;
    source->position = 0;
}
