/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#ifndef H_NANOPNG_STRUCTS
#define H_NANOPNG_STRUCTS

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct nanopng_buffer {
    uint_least32_t size;
    void *block;
} nanopng_buffer_t;

typedef struct nanopng_source {
    uint_least8_t type;
} nanopng_source_t;

typedef struct nanopng_file {
    nanopng_source_t base;

    FILE *file;
} nanopng_file_t;

typedef struct nanopng_buffer_src {
    nanopng_source_t base;

    size_t position;
    nanopng_buffer_t buffer;
} nanopng_buffer_src_t;

typedef struct nanopng_header {
    uint_least32_t width,
                   height;

    uint8_t bit_depth,
            color_type,
            compression_method,
            filter_method,
            interlace_method;
} nanopng_header_t;

typedef struct nanopng_chunk {
    unsigned char name[4];
    nanopng_buffer_t buffer;
    uint_least32_t crc;
} nanopng_chunk_t;

typedef struct nanopng_chunk_queue {
    struct nanopng_chunk_queue *next;
    nanopng_chunk_t chunk;
} nanopng_chunk_queue_t;

typedef struct nanopng {
    nanopng_source_t *source;
    nanopng_header_t header;
    nanopng_chunk_queue_t *chunks;
} nanopng_t;

#endif /* H_NANOPNG_STRUCTS */
