/*
    Copyright (C) 2017 Astie Teddy

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdlib.h>
#include <string.h>

#include "nanopng.h"
#include "nanopng_chunk.h"
#include "nanopng_source.h"

int nanopng_read_chunk(nanopng_source_t *source, nanopng_chunk_t **chunk_ptr)
{
    /* Read chunk name */
    unsigned char chunk_name[4];

    if (nanopng_source_read_block(source, 4, chunk_name))
        return NANOPNG_ENODATA;

    /* Read chunk size */
    uint_least32_t chunk_size;

    if(nanopng_source_read_uint32(source, &chunk_size))
        return NANOPNG_ENODATA;

    /* Allocate chunk and it's buffer in only one malloc */
    nanopng_chunk_t *chunk = malloc(sizeof(nanopng_chunk_t) + chunk_size);

    if (chunk == NULL)
        /* Out of memory */
        return NANOPNG_ENOMEM;

    /* Define all known variables. */
    memcpy(chunk->name, chunk_name, 4);

    /* Get chunk data part of the malloc */
    chunk->buffer.block = (char *)chunk + sizeof(nanopng_chunk_t);

    /* size_t can be thinner than uint_least32_t in rare
       cases, so, check if chunk_size fit into size_t.
    */
    #if SIZE_MAX < UINT_LEAST32_MAX
    if (SIZE_MAX < chunk_size) {
        /* Return it with "unmanageable" error code to avoid an integer overflow */
        free(chunk);
        return NANOPNG_EUNMANAGABLE;
    }
    #endif

    chunk->buffer.size = chunk_size;

    /* Read chunk data */
    if (nanopng_source_read_block(source, chunk->buffer.size, chunk->buffer.block)) {
        /* No more data */
        free(chunk);
        return NANOPNG_ENODATA;
    }

    /* Read CRC */
    if (nanopng_source_read_uint32(source, &chunk->crc)) {
        /* No more data */
        free(chunk);
        return NANOPNG_ENODATA;
    }

    *chunk_ptr = chunk;

    /* Chunk is readed, let's go on. */
    return NANOPNG_OK;
}
